// tạo css như một biến của một ojb các dấu gạch ngang bỏ viết theo kiểu camelCased
import img from "./assets/images/48.jpg"

const devContainer = {
  width: "800px",
  border: "1px solid #ddd",
  margin: "0 auto",
  textAlign: "center",
  marginTop: "100px",
  padding: "0px 0px 50px 0px",
}

const devAvt = {
  margin: "-90px auto 30px",
  borderRadius:"50px",
  width: "100px",
  marginTop: "-50px",
  objectFit : "cover"
  
}
const devWord = {
  fontSize: "18px"
}
const devUser = {
  fontSize: "18px"
}



function App() {
  return (
    <div style={devContainer}>
      <div style={devAvt}>
        <img src={img} alt="ảnh"></img>
      </div>
      <div style={devWord}>
        <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div style={devUser}>
      <b>Tammy Stevens</b> * Front End developer 
      </div>
    </div>
  );
}

export default App;
